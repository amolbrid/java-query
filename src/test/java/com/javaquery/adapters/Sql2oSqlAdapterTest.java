package com.javaquery.adapters;

import static org.assertj.core.api.Assertions.assertThat;

import com.javaquery.domain.User;
import org.junit.Test;

import java.util.List;

public class Sql2oSqlAdapterTest {
  @Test
  public void executesSqlAndGetsResultBack() {
    String cnnString = "jdbc:sqlite:./src/test/resources/testdb.sqlite3";
    Sql2oSqlAdapter sqlAdapter = new Sql2oSqlAdapter(cnnString, "", "");

    List<User> list = sqlAdapter.execute("SELECT * FROM users", User.class);

    assertThat(list.size()).isEqualTo(2);
    assertThat(list).contains(new User(1, "a"), new User(2, "b"));
  }
}
