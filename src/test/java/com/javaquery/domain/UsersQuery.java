package com.javaquery.domain;

import java.util.List;

public interface UsersQuery {
  List<User> getUsers();
}
