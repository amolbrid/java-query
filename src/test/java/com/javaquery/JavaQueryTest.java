package com.javaquery;

import static org.assertj.core.api.Assertions.assertThat;

import com.javaquery.adapters.Sql2oSqlAdapter;
import com.javaquery.domain.User;
import com.javaquery.domain.UsersQuery;
import org.junit.Test;

import java.util.List;

public class JavaQueryTest {
  @Test
  public void canGetAllUsersFromDatabase() {
    String cnnString = "jdbc:sqlite:./src/test/resources/testdb.sqlite3";
    Sql2oSqlAdapter sqlAdapter = new Sql2oSqlAdapter(cnnString, "", "");
    JavaQuery javaQuery = new JavaQuery(sqlAdapter);

    UsersQuery userQuery = javaQuery.build(UsersQuery.class);
    List<User> list = userQuery.getUsers();

    assertThat(list.size()).isEqualTo(2);
    assertThat(list).contains(new User(1, "a"), new User(2, "b"));
  }
}
