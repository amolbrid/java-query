package com.javaquery.adapters;

import org.sql2o.*;

import java.util.List;

/**
 * {@code SqlAdapter} for Sql2o (http://www.sql2o.org/) database query library.
 *
 * @author Amol Brid.
 */
public class Sql2oSqlAdapter implements SqlAdapter {
  private Sql2o sql2o;

  public Sql2oSqlAdapter(String url, String username, String password) {
    sql2o = new Sql2o(url, username, password);
  }

  @Override
  public <T> List<T> execute(String sql, Class<T> klass) {
    try (Connection con = sql2o.open()) {
      org.sql2o.Query query = con.createQuery(sql).throwOnMappingFailure(false);

      return query.executeAndFetch(klass);
    }
  }
}
