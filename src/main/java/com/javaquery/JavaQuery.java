package com.javaquery;

import com.javaquery.adapters.SqlAdapter;

import java.lang.reflect.Proxy;

public class JavaQuery {
  private final SqlAdapter sqlAdapter;

  public JavaQuery(SqlAdapter sqlAdapter) {
    this.sqlAdapter = sqlAdapter;
  }

  public <T> T build(Class<T> type) {
    return (T) Proxy.newProxyInstance(type.getClassLoader(), new Class[] { type }, new MethodHandler(sqlAdapter));
  }
}
