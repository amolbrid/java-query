# JavaQuery

Prototype project to convert java methods to SQL statement and execute it against database.  

[![build status](https://gitlab.com/amolbrid/java-query/badges/master/build.svg)](https://gitlab.com/amolbrid/java-query/commits/master) [![coverage report](https://gitlab.com/amolbrid/java-query/badges/master/coverage.svg)](https://gitlab.com/amolbrid/java-query/commits/master)
### Usage

Define interface with methods to query database.

```java
public interface UsersQuery {
  List<User> getUsers();
}
```

Then use ```JavaQuery``` class to query database:

```java
String cnnString = "jdbc:sqlite:./src/test/resources/testdb.sqlite3";
Sql2oSqlAdapter sqlAdapter = new Sql2oSqlAdapter(cnnString, "", "");

JavaQuery javaQuery = new JavaQuery(sqlAdapter);
UsersQuery userQuery = javaQuery.build(UsersQuery.class);
List<User> list = userQuery.getUsers();
for (User user : list) {
  System.out.println(user.name);
}
```

```User``` class is defined as:

```java
public class User {
  public int id;
  public String name;

  public User() { }

  public User(int id, String name) {
    this.id = id;
    this.name = name;
  }
}
```
